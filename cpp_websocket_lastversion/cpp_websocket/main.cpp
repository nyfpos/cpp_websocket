#include "src\pch.h"
#include "src\WsServer.h"
#include <fstream>
#include <sstream>
#include <string>

class SocketHandler
{
public:
	void onAcceptSocket(Websocket* socket)
	{
		socket->onMessage(boost::bind(&SocketHandler::onReciveMessage, this, _1, _2));
	}
	void onReciveMessage(Websocket* socket, WebsocketData msg)
	{
		static int count = 0;		
		if (msg.len > 0)
		{	
			char cnt[12] = { 0 };
			_itoa_s(count++, cnt, 10);
			std::string filename = std::string("tmp\\server[") + socket->id() + "]" + cnt + ".json";
			std::ofstream myfile;
			myfile.open(filename.c_str());
			myfile << std::string(msg.data, msg.len);
			myfile.close();
			socket->send((Byte*)msg.data, msg.len);
		}
	}
	void foo(const char* str)
	{
		printf("%s\n", str);
	}
};

int main()
{
	SocketHandler helper;
	//void(SocketHandler::*fn)(const char*) = &SocketHandler::foo;
	wsserver_config conf = { 
		0, 
		"127.0.0.1", 
		7681, 
		NULL, 
		NULL
	};


	WsServer::start(conf);
	WsServer::setAcceptSocketCallback(boost::bind(&SocketHandler::onAcceptSocket, &helper, _1));

	//(helper.*fn)("Test for stored function call.");

	MSG msg;	
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	WsServer::shutdown();

	return 0;
}