#pragma once

typedef unsigned char Byte;


#define MAX_WEBSOCKET_RECV_SIZE 512
#define _RX_BUFFER_SIZE_ 65536

#define WA_LWS_PORT 7681
#define WA_LWS_IFACE "127.0.0.1"

extern "C"
{
#include "lws_config.h"
#include "libwebsockets.h"
	//#include "util\getopt.h"
	//#include "util\gettimeofday.h"
}

#include <sys/types.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <map>
#include <list>
#include <queue>
#include <vector>
#include <thread>
#include <atomic>
#include <boost/thread/thread.hpp>
#include <boost\function.hpp>
#include <boost/shared_ptr.hpp>
#include "util\Lock.h"

class Websocket;
typedef boost::shared_ptr<Websocket> WebsocketPtr;

extern const unsigned __int64 epoch;
int gettimeofday(struct timeval * tp, struct timezone * tzp);