#include "Websocket.h"

Websocket::Websocket(lws* socket) : _recv(MAX_WEBSOCKET_RECV_SIZE), _socket(socket)
{
	if (_socket)
	{
		sprintf_s(_wsid, "%p", _socket);
	}
}
Websocket::~Websocket() 
{ 

}

const char* Websocket::id()
{
	return _wsid;
}

int Websocket::_OnWsESTABLISHED(struct lws *wsi, void *user, void *in, size_t len)
{
	printf("socket(%p) ws established\n", wsi);
	return 0;
}

int Websocket::_OnWsCLOSED(struct lws *wsi, void *user, void *in, size_t len)
{
	printf("socket(%p) ws closed\n", wsi);
	return 0;
}

int Websocket::_OnWsRECEIVE(struct lws *wsi, void *user, void *in, size_t len)
{
	__try
	{		
		_recv.write((unsigned char*)in, len);
		const size_t remaining = lws_remaining_packet_payload(wsi);
		if (!remaining && lws_is_final_fragment(wsi)) 
		{
			if (!_onMessageCb.empty())
			{
				_onMessageCb(this, data());
				_recv.clear();
			}
		}
	}
	__except (StackTracer::ExceptionFilter(GetExceptionInformation()))
	{
		StackTracer_HandleException();
	}
	return 0;
}

int Websocket::_OnWsWRITEABLE(struct lws *wsi, void *user, void *in, size_t len_)
{
	printf("socket(%p) ws writable\n", wsi);
	if (_qsend.size() == 0)
		return 0;
	
	bool failed = false;
	__try
	{		
		WsStream*  psend = _qsend.front();
		int len = psend->size(), n = 0;
		printf("socket(%p) ws write prepare len(%d)\n", wsi, len);
		if (len > 0)
		{
			n = lws_write(wsi, psend->begin(), len, LWS_WRITE_BINARY);
			if (n < 0) {
				printf("Failed write LWS_CALLBACK_CLIENT_WRITEABLE(n:%d,len:%d)\n", n, len);
				return -1;
			}
			else {
				psend->drop(n);
			}
		}
		if (psend->size() == 0)
		{
			_qsend.pop();
			delete psend;
		}
	}
	__except (StackTracer::ExceptionFilter(GetExceptionInformation()))
	{
		StackTracer_HandleException();
		failed = true;
	}
	if (failed)
	{
		printf("dead\n");
	}
	return 0;
}

int Websocket::_OnWsConnError(struct lws *wsi, void *user, void *in, size_t len)
{
	printf("socket(%p) ws conn error\n", wsi);
	return 0;
}

WebsocketData Websocket::data()
{
	return
	{
		(char*)_recv.begin(), 
		_recv.size()
	};	
}

void Websocket::send(Byte* buffer, int len)
{
	{		
		WsStream* psend =new WsStream(len);
		psend->write(buffer, len);
		_qsend.push(psend);		
	}
}

int Websocket::read(Byte* buffer, int len)
{
	int n = 0;
	{		
		int validDataSize = _recv.size();
		if (validDataSize >= len)
			n = len;
		else
			n = validDataSize;
		if (n > 0)
		{
			n = _recv.read(buffer, n);
		}
	}
	return n;
}
