#include "WsServer.h"

boost::thread_group WsServer::_thread;
boost::function<void(Websocket*)> WsServer::_acceptCb;
std::atomic<bool> WsServer::_startService = false;
swlib::CriticalSession WsServer::_libwebLock;
swlib::CriticalSession WsServer::_mapLock;
lws_context* WsServer::_context = NULL;
std::map<int, WebsocketPtr> WsServer::_websockets;
wsserver_config WsServer::_conf;

WsServer::WsServer()
{

}

WsServer::~WsServer()
{

}

void WsServer::_dumpHandshakeInfo(struct lws *wsi)
{
	int n = 0, len;
	char buf[256];
	const unsigned char *c;

	do {
		c = lws_token_to_string((enum lws_token_indexes)n);
		if (!c) {
			n++;
			continue;
		}

		len = lws_hdr_total_length(wsi, (enum lws_token_indexes)n);
		if (!len || len > sizeof(buf) - 1) {
			n++;
			continue;
		}

		lws_hdr_copy(wsi, buf, sizeof buf, (enum lws_token_indexes)n);
		buf[sizeof(buf) - 1] = '\0';

		fprintf(stderr, "    %s = %s\n", (char *)c, buf);
		n++;
	} while (c);
}

void WsServer::_OnServiceLoop()
{
	int use_ssl = _conf.use_ssl;
	int opts = 0;

	opts |= LWS_SERVER_OPTION_LIBEV;
	opts |= LWS_SERVER_OPTION_ALLOW_NON_SSL_ON_SSL_PORT;

	lws_context_creation_info info;

	memset(&info, 0, sizeof info);	
	info.port = _conf.port;
	info.iface = _conf.iface;
	info.protocols = protocols;

#ifndef LWS_NO_EXTENSIONS
	info.extensions = lws_get_internal_extensions();
#endif

	if (!use_ssl || _conf.cert_path == NULL) {
		info.ssl_cert_filepath = NULL;
		info.ssl_private_key_filepath = NULL;
	}
	else {
		// load pem file...
		info.ssl_cert_filepath = _conf.cert_path;
		info.ssl_private_key_filepath = _conf.key_path;
	}

	info.gid = -1;
	info.uid = -1;
	info.max_http_header_pool = 16;
	info.options = opts | LWS_SERVER_OPTION_VALIDATE_UTF8;
	info.timeout_secs = 60*60;
	info.extensions = exts;
	// info.count_threads = _conf.count_threads;
	/*
	info.ssl_cipher_list = "ECDHE-ECDSA-AES256-GCM-SHA384:"
		"ECDHE-RSA-AES256-GCM-SHA384:"
		"DHE-RSA-AES256-GCM-SHA384:"
		"ECDHE-RSA-AES256-SHA384:"
		"HIGH:!aNULL:!eNULL:!EXPORT:"
		"!DES:!MD5:!PSK:!RC4:!HMAC_SHA1:"
		"!SHA1:!DHE-RSA-AES128-GCM-SHA256:"
		"!DHE-RSA-AES128-SHA256:"
		"!AES128-GCM-SHA256:"
		"!AES128-SHA256:"
		"!DHE-RSA-AES256-SHA256:"
		"!AES256-GCM-SHA384:"
		"!AES256-SHA256";
		*/
	_context = lws_create_context(&info);
	if (_context == NULL) {
		printf("lws init failed\n");
		return ;
	}

	//unsigned int ms, oldms = 0;
	int n = 0;
	while (_startService && n >= 0)
	{
		/*
		struct timeval tv;

		gettimeofday(&tv, NULL);

		ms = (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
		if ((ms - oldms) > 50) {
			lws_callback_on_writable_all_protocol(&protocols[PROTOCOL_DUMB_INCREMENT]);
			oldms = ms;
		}*/

		Sleep(1);
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			n = lws_service(_context, 50);
		}
	}

	lws_context_destroy(_context);
	printf("lws-test-server exited cleanly\n");
}

void WsServer::start(wsserver_config conf)
{
	_conf = conf;
	_startService = true;
	_thread.create_thread(WsServer::_OnServiceLoop);
}

void WsServer::shutdown()
{
	_startService = false;
	_thread.join_all();
}

void WsServer::setAcceptSocketCallback(boost::function<void(Websocket*)> acceptCb)
{
	_acceptCb = acceptCb;
}

int WsServer::callback_http(struct lws *wsi, enum lws_callback_reasons reason, 
	                        void *user, void *in, size_t len)
{
	switch (reason) {
	case LWS_CALLBACK_CLIENT_WRITEABLE:
		printf("connection established\n");
	case LWS_CALLBACK_HTTP: {
		char *requested_uri = (char *)in;
		printf("requested URI: %s\n", requested_uri);
		const char* universal_response = "";
		lws_write(wsi, (unsigned char*)universal_response,	strlen(universal_response), LWS_WRITE_HTTP);
		lws_callback_on_writable(wsi);		
	} break;
	case LWS_CALLBACK_HTTP_BODY_COMPLETION:{
		lws_callback_on_writable(wsi);
	} break;
	default:		
		break;
	}
	return 0;
}

int WsServer::callback_lws(struct lws *wsi, enum lws_callback_reasons reason, 
	                       void *user, void *in, size_t len)
{	
	switch (reason) {

	case LWS_CALLBACK_ESTABLISHED:
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)				
			WebsocketPtr pSocket = _websockets[lws_get_socket_fd(wsi)] = WebsocketPtr(new Websocket(wsi));

			if (!_acceptCb.empty())
			{
				_acceptCb(pSocket.get());
			}
			pSocket->_OnWsESTABLISHED(wsi, user, in, len);
		} 		
		break;
	case LWS_CALLBACK_CLOSED:
	case LWS_CALLBACK_PROTOCOL_DESTROY:
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)			
			std::map<int, WebsocketPtr> ::iterator it = _websockets.find(lws_get_socket_fd(wsi));
			if (it != _websockets.end()) 
			{
				WebsocketPtr pSocket = it->second;
				pSocket->_OnWsCLOSED(wsi, user, in, len);
				_websockets.erase(it);
			}
		} break;
	case LWS_CALLBACK_SERVER_WRITEABLE:
		{			
			THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			
			WebsocketPtr pSocket = _websockets[lws_get_socket_fd(wsi)];
			pSocket->_OnWsWRITEABLE( wsi, user, in, len);			
			lws_rx_flow_allow_all_protocol(lws_get_context(wsi), lws_get_protocol(wsi));
			if (lws_partial_buffered(wsi) || lws_send_pipe_choked(wsi)) 
				lws_callback_on_writable( wsi);
		} break;
	case LWS_CALLBACK_RECEIVE:
	{	
			THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)			
			WebsocketPtr pSocket = _websockets[lws_get_socket_fd(wsi)];
			pSocket->_OnWsRECEIVE(wsi, user, in, len);
		/*
			lws_rx_flow_control(wsi, 0);
			lws_callback_on_writable_all_protocol(lws_get_context(wsi), lws_get_protocol(wsi));*/			
			lws_callback_on_writable(wsi);
		} break;
	case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
		_dumpHandshakeInfo(wsi);
		break;
	default:
		break;
	}

	return 0;
}
