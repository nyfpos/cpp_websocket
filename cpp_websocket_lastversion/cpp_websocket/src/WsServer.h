#pragma once

#include "pch.h"
#include "Websocket.h"

struct per_session_data__http {
	lws_filefd_type fd;
#ifdef LWS_WITH_CGI
	struct lws_cgi_args args;
#endif
#if defined(LWS_WITH_CGI) || !defined(LWS_NO_CLIENT)
	int reason_bf;
#endif
	unsigned int client_finished : 1;
};

struct wsserver_config
{
	int use_ssl;
	char* iface;
	int port;
	char* cert_path;
	char* key_path;
};

class WsServer
{

private:
	static std::map<int, WebsocketPtr> _websockets;

	static swlib::CriticalSession _libwebLock;
	static swlib::CriticalSession _mapLock;
	static void _OnServiceLoop();
	static std::atomic<bool> _startService;
	static boost::thread_group _thread;	
	static lws_context *_context;
	static boost::function<void(Websocket*)> _acceptCb;
	static wsserver_config _conf;
	static void _dumpHandshakeInfo(struct lws *wsi);
public:

	WsServer();
	~WsServer();

	static void start(wsserver_config conf);
	static void shutdown();

	static void setAcceptSocketCallback(boost::function<void(Websocket*)> acceptCb);

	static Websocket* getInstance(struct libwebsocket *wsi);
	static int callback_lws(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);
	static int callback_http(struct lws *wsi, enum lws_callback_reasons reason, void *user,	void *in, size_t len);
};

static const struct lws_extension exts[] = {
/*	{
		"permessage-deflate",
		lws_extension_callback_pm_deflate,
		"permessage-deflate"
	},
	{
		"deflate-frame",
		lws_extension_callback_pm_deflate,
		"deflate_frame"
	},*/
	{ NULL, NULL, NULL /* terminator */ }
};

static struct lws_protocols protocols[] =
{/*
	{
		"http-only",		
		WsServer::callback_http,
		sizeof(struct per_session_data__http),
		0,
	},*/
	{
		"default",//"lws-mirror-protocol",//"fake-nonexistant-protocol,lws-mirror-protocol",
		WsServer::callback_lws,
		0,
		_RX_BUFFER_SIZE_,
	},
	{ NULL, NULL, 0, 0 }
};