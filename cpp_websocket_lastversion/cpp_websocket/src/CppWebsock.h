#pragma once
#include "pch.h"
#include "WsStream.h"


struct ServiceInfo;

class CppWebsock
{		
private:
	struct libwebsocket_context *_context;		
	bool _beClose;
	bool _beWritable;
	int _curState;	

	static swlib::CriticalSession _mapLock;
	static swlib::CriticalSession _libwebLock;
	swlib::CriticalSession _sendLock;
	swlib::CriticalSession _readLock;

	static std::map<int, CppWebsock*> _mapInstance;
	
	WsStream _send;
	WsStream _recv;

	std::atomic<bool> _connected;	
	std::atomic<bool> _ready;
	std::atomic<bool> _connectFailed;

	int _OnWsESTABLISHED(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);
	int _OnWsCLOSED(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);
	int _OnWsRECEIVE(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);
	int _OnWsWRITEABLE(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);	
	int _OnWsConnError(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);	
	static void _OnServiceLoop();
	

	static swlib::CriticalSession _addLock;
	static swlib::CriticalSession _rmLock;
	static std::list<ServiceInfo*> _addToService;
	static std::list<ServiceInfo*> _rmFromService;
	static std::atomic<bool> _startService;
	static boost::thread_group _serviceThread;	

public:
	CppWebsock();
	~CppWebsock();
	bool isConnected() const { return _connected; }
	bool isConnectFailed() const { return _connectFailed; }
	int connect(const char *address, int port, int ssl_connection, const char *path, const char *host, const char *origin, int ietf_version_or_minus_on);
	int read(unsigned char* buffer, int len, int timeout_ms);	
	int write(unsigned char* buffer, int len, int timeout);
	void disconnect();


	static void start();
	static void shutdown();
	static CppWebsock* getInstance(struct libwebsocket *wsi);
	static int callback_lws(struct libwebsocket_context *context,
	                        struct libwebsocket *wsi,
	                        enum libwebsocket_callback_reasons reason,
		                    void *user, void *in, size_t len);	
};

struct ServiceInfo
{
	ServiceInfo(CppWebsock* ws_, int socket_) :ws(ws_), socket(socket_) {}
	CppWebsock* ws;
	int socket;
};


