#pragma once

#include "pch.h"
#include "WsStream.h"
#include"util\StackTracer.h"

struct WebsocketData
{
	const char* data;
	int len;
};

class WsServer;

class Websocket
{
private:
	swlib::CriticalSession _sendLock;
	swlib::CriticalSession _readLock;
	lws *_socket;
	char _wsid[10];
	std::queue<WsStream*> _qsend;
	//WsStream _send;
	WsStream _recv;
	boost::function<void(Websocket*, WebsocketData)> _onMessageCb;


	int _OnWsESTABLISHED(struct lws *wsi, void *user, void *in, size_t len);
	int _OnWsCLOSED(struct lws *wsi, void *user, void *in, size_t len);
	int _OnWsRECEIVE(struct lws *wsi, void *user, void *in, size_t len);
	int _OnWsWRITEABLE(struct lws *wsi, void *user, void *in, size_t len);
	int _OnWsConnError(struct lws *wsi, void *user, void *in, size_t len);


public:
	friend class WsServer;

	Websocket(lws* socket);
	~Websocket();

	WebsocketData data();

	const char* id();
	void send(Byte* buffer, int len);
	int read(Byte* buffer, int len);

	void onMessage(boost::function<void(Websocket*, WebsocketData)> onMessageCb)
	{
		_onMessageCb = onMessageCb;
	}
};