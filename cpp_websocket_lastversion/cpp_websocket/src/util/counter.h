#pragma once
#include <winsock2.h>

class Countdown
{
private:
	int gettimeofday(struct timeval * tp, struct timezone * tzp)
	{
		const unsigned __int64 epoch = ((unsigned __int64)116444736000000000ULL);
		FILETIME    file_time;
		SYSTEMTIME  system_time;
		ULARGE_INTEGER ularge;

		GetSystemTime(&system_time);
		SystemTimeToFileTime(&system_time, &file_time);
		ularge.LowPart = file_time.dwLowDateTime;
		ularge.HighPart = file_time.dwHighDateTime;

		tp->tv_sec = (long)((ularge.QuadPart - epoch) / 10000000L);
		tp->tv_usec = (long)(system_time.wMilliseconds * 1000);

		return 0;
	}

	void timersub(const struct timeval* tvp, const struct timeval* uvp, struct timeval* vvp)
	{
		vvp->tv_sec = tvp->tv_sec - uvp->tv_sec;
		vvp->tv_usec = tvp->tv_usec - uvp->tv_usec;
		if (vvp->tv_usec < 0)
		{
			--vvp->tv_sec;
			vvp->tv_usec += 1000000;
		}
	}

	void timeradd(const struct timeval* tvp, const struct timeval* uvp, struct timeval* vvp)
	{
		vvp->tv_sec = tvp->tv_sec + uvp->tv_sec;
		vvp->tv_usec = tvp->tv_usec + uvp->tv_usec;
		if (vvp->tv_usec >= 1000000)
		{
			++vvp->tv_sec;
			vvp->tv_usec -= 1000000;
		}
	}

public:
	Countdown()
	{

	}

	Countdown(int ms)
	{
		countdown_ms(ms);
	}


	bool expired()
	{
		struct timeval now, res;
		gettimeofday(&now, NULL);
		timersub(&end_time, &now, &res);
		//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
		//if (res.tv_sec > 0 || res.tv_usec > 0)
		//	printf("expired %d %d\n", res.tv_sec, res.tv_usec);
		return res.tv_sec < 0 || (res.tv_sec == 0 && res.tv_usec <= 0);
	}


	void countdown_ms(int ms)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		struct timeval interval = { ms / 1000, (ms % 1000) * 1000 };
		//printf("interval %d %d\n", interval.tv_sec, interval.tv_usec);
		timeradd(&now, &interval, &end_time);
	}


	void countdown(int seconds)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		struct timeval interval = { seconds, 0 };
		timeradd(&now, &interval, &end_time);
	}


	int left_ms()
	{
		struct timeval now, res;
		gettimeofday(&now, NULL);
		timersub(&end_time, &now, &res);
		//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
		return (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000;
	}

private:

	struct timeval end_time;
};