/*******************************************************************************
 * Copyright (c) 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Ian Craggs - initial API and implementation and/or initial documentation
 *******************************************************************************/

#include <sys/types.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <stdlib.h>
#include <string.h>
#include <signal.h>



class IPStack 
{
public:    
    IPStack()
    {

    }
    
	int Socket_error(const char* aString)
	{
		int rc = 0;
		//if (errno != EINTR && errno != EAGAIN && errno != EINPROGRESS && errno != EWOULDBLOCK)
		//{
			if (strcmp(aString, "shutdown") != 0 || (errno != ENOTCONN && errno != ECONNRESET))
			{
				printf("Socket error %s in %s for socket %d\n", strerror(errno), aString, mysock);
				rc = errno;
			}
		//}
		return errno;
	}

    int connect(const char* hostname, int port)
    {
		WSADATA wsaData;
		struct addrinfo *result = NULL, *ptr = NULL;
		int iResult;
		struct sockaddr_in address;
		ADDRESS_FAMILY family = AF_INET;

		mysock = INVALID_SOCKET;

		// Initialize Winsock
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d\n", iResult);
			return 1;
		}

		struct addrinfo hints = { 0, AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP, 0, NULL, NULL, NULL };

		// Resolve the server address and port
		iResult = getaddrinfo(hostname, NULL, &hints, &result);
		if (iResult != 0) {
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
			return 1;
		}
		else
		{
			struct addrinfo* res = result;

			/* prefer ip4 addresses */
			while (res)
			{
				if (res->ai_family == AF_INET)
				{
					result = res;
					break;
				}
				res = res->ai_next;
			}

			if (result->ai_family == AF_INET)
			{
				address.sin_port = htons(port);
				address.sin_family = family = AF_INET;
				address.sin_addr = ((struct sockaddr_in*)(result->ai_addr))->sin_addr;
			}
			else
			{
				printf("getaddrinfo failed with error: %d\n", iResult);
				WSACleanup();
				return 1;
			}

			freeaddrinfo(result);
		}

		if (iResult == 0)
		{
			mysock = socket(family, SOCK_STREAM, 0);
			if (mysock != -1)
			{
				int opt = 1;
				iResult = ::connect(mysock, (struct sockaddr*)&address, sizeof(address));
			}
		}

		if (mysock == INVALID_SOCKET) {
			printf("Unable to connect to server!\n");
			WSACleanup();
			return 1;
		}
		return iResult;
    }

    int read(unsigned char* buffer, int len, int timeout_ms)
    {
		struct timeval interval = {timeout_ms / 1000, (timeout_ms % 1000) * 1000};
		if (interval.tv_sec < 0 || (interval.tv_sec == 0 && interval.tv_usec <= 0))
		{
			interval.tv_sec = 0;
			interval.tv_usec = 100;
		}

		setsockopt(mysock, SOL_SOCKET, SO_RCVTIMEO, (char *)&interval, sizeof(struct timeval));

		int bytes = 0;
		while (bytes < len)
		{
			int rc = ::recv(mysock, (char*)&buffer[bytes], (size_t)(len - bytes), 0);
			if (rc == -1)
			{
				if (Socket_error("read") != 0)
				{
					bytes = -1;
					break;
				}
			}
			else
				bytes += rc;
		}
		return bytes;
    }
    
    int write(unsigned char* buffer, int len, int timeout)
    {
		struct timeval tv;

		tv.tv_sec = 0;  /* 30 Secs Timeout */
		tv.tv_usec = timeout * 1000;  // Not init'ing this can cause strange errors

		setsockopt(mysock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
		int	rc = ::send(mysock, (char*)buffer, len, 0);
		//printf("write rc %d\n", rc);
		return rc;
    }

	int disconnect()
	{
		// shutdown the connection since no more data will be sent
		int iResult = shutdown(mysock, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			printf("shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(mysock);
			WSACleanup();
		}

		// cleanup
		closesocket(mysock);
		WSACleanup();
		return iResult;
	}
    
private:

    int mysock; 
    
};


class Countdown
{
private:
	int gettimeofday(struct timeval * tp, struct timezone * tzp)
	{
		const unsigned __int64 epoch = ((unsigned __int64)116444736000000000ULL);
		FILETIME    file_time;
		SYSTEMTIME  system_time;
		ULARGE_INTEGER ularge;

		GetSystemTime(&system_time);
		SystemTimeToFileTime(&system_time, &file_time);
		ularge.LowPart = file_time.dwLowDateTime;
		ularge.HighPart = file_time.dwHighDateTime;

		tp->tv_sec = (long)((ularge.QuadPart - epoch) / 10000000L);
		tp->tv_usec = (long)(system_time.wMilliseconds * 1000);

		return 0;
	}

	void timersub(const struct timeval* tvp, const struct timeval* uvp, struct timeval* vvp)
	{
		vvp->tv_sec = tvp->tv_sec - uvp->tv_sec;
		vvp->tv_usec = tvp->tv_usec - uvp->tv_usec;
		if (vvp->tv_usec < 0)
		{
			--vvp->tv_sec;
			vvp->tv_usec += 1000000;
		}
	}

	void timeradd(const struct timeval* tvp, const struct timeval* uvp, struct timeval* vvp)
	{
		vvp->tv_sec = tvp->tv_sec + uvp->tv_sec;
		vvp->tv_usec = tvp->tv_usec + uvp->tv_usec;
		if (vvp->tv_usec >= 1000000)
		{
			++vvp->tv_sec;
			vvp->tv_usec -= 1000000;
		}
	}

public:
    Countdown()
    { 
	
    }

    Countdown(int ms)
    { 
		countdown_ms(ms);
    }
    

    bool expired()
    {
		struct timeval now, res;
		gettimeofday(&now, NULL);
		timersub(&end_time, &now, &res);		
		//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
		//if (res.tv_sec > 0 || res.tv_usec > 0)
		//	printf("expired %d %d\n", res.tv_sec, res.tv_usec);
        return res.tv_sec < 0 || (res.tv_sec == 0 && res.tv_usec <= 0);
    }
    

    void countdown_ms(int ms)  
    {
		struct timeval now;
		gettimeofday(&now, NULL);
		struct timeval interval = {ms / 1000, (ms % 1000) * 1000};
		//printf("interval %d %d\n", interval.tv_sec, interval.tv_usec);
		timeradd(&now, &interval, &end_time);
    }

    
    void countdown(int seconds)
    {
		struct timeval now;
		gettimeofday(&now, NULL);
		struct timeval interval = {seconds, 0};
		timeradd(&now, &interval, &end_time);
    }

    
    int left_ms()
    {
		struct timeval now, res;
		gettimeofday(&now, NULL);
		timersub(&end_time, &now, &res);
		//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
        return (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000;
    }
    
private:

	struct timeval end_time;
};

