#include "CppWebsock.h"

std::atomic<bool> CppWebsock::_startService = false;
std::map<int, CppWebsock*> CppWebsock::_mapInstance;
swlib::CriticalSession CppWebsock::_mapLock;
swlib::CriticalSession CppWebsock::_libwebLock;

swlib::CriticalSession CppWebsock::_addLock;
swlib::CriticalSession CppWebsock::_rmLock;
std::list<ServiceInfo*> CppWebsock::_addToService;
std::list<ServiceInfo*> CppWebsock::_rmFromService;
boost::thread_group CppWebsock::_serviceThread;

static struct libwebsocket_protocols protocols[] = 
{
	{
		"default",//"lws-mirror-protocol",//"fake-nonexistant-protocol,lws-mirror-protocol",
		CppWebsock::callback_lws,
		0,
		_RX_BUFFER_SIZE_,
	},
	{ NULL, NULL, 0, 0 }
};

CppWebsock::CppWebsock() : _beClose(true), _curState(-1), _beWritable(false), _connected(false), _ready(false), _connectFailed(false), _context(NULL), _recv(MAX_WEBSOCKET_RECV_SIZE), _send(MAX_WEBSOCKET_RECV_SIZE)
{	

}


CppWebsock::~CppWebsock()
{

}

CppWebsock* CppWebsock::getInstance(struct libwebsocket *wsi)
{
	if (wsi )
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
		THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
		if(_mapInstance.find(libwebsocket_get_socket_fd(wsi)) != _mapInstance.end())		
			return _mapInstance[libwebsocket_get_socket_fd(wsi)];
	}
	return NULL;
}

int CppWebsock::_OnWsESTABLISHED(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len)
{
	THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
	_connected = true;
	//unsigned int rands[4];
	fprintf(stderr, "callback_lws: LWS_CALLBACK_CLIENT_ESTABLISHED\n");
	//libwebsockets_get_random(context, rands, sizeof(rands[0]));
	libwebsocket_callback_on_writable(context, wsi);
	return 0;
}

int CppWebsock::_OnWsConnError(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len)
{
	_connectFailed = true;
	_connected = false;
	_ready = false;
	fprintf(stderr, "LWS_CALLBACK_CLIENT_CONNECTION_ERROR\n");
	THREAD_LIB_LOCK_CRITICAL_SESSION(_rmLock)
	THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
	_rmFromService.push_back(new ServiceInfo(this, libwebsocket_get_socket_fd(wsi)));
	return 0;
}

int CppWebsock::_OnWsCLOSED(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len)
{
	_connected = false;
	_ready = false;
	fprintf(stderr, " LWS_CALLBACK_CLOSED \n");
	THREAD_LIB_LOCK_CRITICAL_SESSION(_rmLock)
	THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
	_rmFromService.push_back(new ServiceInfo(this, libwebsocket_get_socket_fd(wsi)));
	return 0;
}

int CppWebsock::_OnWsRECEIVE(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len)
{	
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_readLock)
		_recv.write((unsigned char*)in, len);	
		/*
		//============= TRACE =================//
		Byte* trace = _recv.begin();
		int n = _recv.size();
		printf("buffer write[%d]: ", n);
		for (int i = 0; i < n; i++)
			printf("%c", *trace++);
		printf("\n");
		//============= TRACE =================//		
		*/
	}
	return 0;
}

int CppWebsock::_OnWsWRITEABLE(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len)
{	
	bool next = true;
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_sendLock)
		int max_writeable_size = 65535;
		int len = _send.size(), n  =0;	
		len = len > max_writeable_size ? max_writeable_size : len;
		if (len > 0)
		{			
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			unsigned char* buf = new unsigned char[LWS_SEND_BUFFER_PRE_PADDING + len + LWS_SEND_BUFFER_POST_PADDING];
			_send.read(buf + LWS_SEND_BUFFER_PRE_PADDING, len);
			n = libwebsocket_write(wsi, buf + LWS_SEND_BUFFER_PRE_PADDING, len, LWS_WRITE_BINARY);			
			delete[] buf;
		}
		if (n < len || n < 0) {
			lwsl_err("Partial write LWS_CALLBACK_CLIENT_WRITEABLE\n");
			next = false;
		}
	}
	if (!next)
		return -1;
	_beWritable = true;
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
		libwebsocket_callback_on_writable(context, wsi);
	}
	return 0;
}

int CppWebsock::callback_lws(struct libwebsocket_context *context,
                                    struct libwebsocket *wsi,
                                    enum libwebsocket_callback_reasons reason,
	                                void *user, void *in, size_t len)
{
	CppWebsock* pws = CppWebsock::getInstance(wsi);
	if (pws == NULL) return 0;
	pws->_curState = (int)reason;
	switch (reason) 
	{
	case LWS_CALLBACK_CLIENT_ESTABLISHED:
		return pws->_OnWsESTABLISHED(context, wsi, user, in, len);
	case LWS_CALLBACK_CLOSED:
		return pws->_OnWsCLOSED(context, wsi, user, in, len);
	case LWS_CALLBACK_CLIENT_RECEIVE:
		return pws->_OnWsRECEIVE(context, wsi, user, in, len);
	case LWS_CALLBACK_CLIENT_WRITEABLE:
		return pws->_OnWsWRITEABLE(context, wsi, user, in, len);
	case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:		
		return pws->_OnWsConnError(context, wsi, user, in, len);		
	default:
		break;
	}
	return 0;
}

int CppWebsock::connect(const char *address, int port, int ssl_connection, const char *path, const char *host, const char *origin, int ietf_version_or_minus_on)
{
	if (_context == NULL)
	{
		struct lws_context_creation_info info;
		memset(&info, 0, sizeof info);
		info.port = CONTEXT_PORT_NO_LISTEN;
		info.protocols = protocols;
#ifndef LWS_NO_EXTENSIONS
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			info.extensions = libwebsocket_get_internal_extensions();
		}
#endif
		info.gid = -1;
		info.uid = -1;
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			_context = libwebsocket_create_context(&info);
		}
		if (_context == NULL) {
			fprintf(stderr, "Creating libwebsocket context failed\n");
			return -1;
		}
	}
	struct libwebsocket *wsi = NULL;
	int n = 0;
	while (n >= 0) 
	{
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			n = libwebsocket_service(_context, 10);
		}
		if (n < 0)
			continue;
		//		printf("n:%d ssl:%d version:%d protocols_name:%s\n", n, ssl_connection, ietf_version_or_minus_on, protocols[0].name);
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
			wsi = libwebsocket_client_connect(_context, address, port, ssl_connection, path, host, origin, protocols[0].name, ietf_version_or_minus_on);
		}
		if (wsi == NULL) 
		{
			fprintf(stderr, "libwebsocket connect failed\n");
			return -1;
		}
		else
		{
			_connectFailed = false;
			THREAD_LIB_LOCK_CRITICAL_SESSION(_addLock)
			THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)			
			_addToService.push_back(new ServiceInfo(this, libwebsocket_get_socket_fd(wsi)));
			break;
		}
	}
	_ready = true;
	return 0;
}

void CppWebsock::_OnServiceLoop()
{		
	while (_startService)
	{
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
			THREAD_LIB_LOCK_CRITICAL_SESSION(_addLock)
			while (_addToService.size() > 0)
			{
				if (!_startService)
					break;
				ServiceInfo* info = _addToService.front();
				_addToService.pop_front();
				if (info)
				{
					_mapInstance[info->socket] = info->ws;
					delete info;
				}
			}
			THREAD_LIB_LOCK_CRITICAL_SESSION(_rmLock)
			while (_rmFromService.size() > 0)
			{
				if (!_startService)
					break;
				ServiceInfo* info = _rmFromService.front();
				_rmFromService.pop_front();
				if (info)
				{
					std::map<int, CppWebsock*>::iterator it = _mapInstance.find(info->socket);
					if (it != _mapInstance.end())
						_mapInstance.erase(it);
					delete info;
				}
			}
		}
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
			for (std::map<int, CppWebsock*>::iterator it = _mapInstance.begin(); it != _mapInstance.end(); it++)
			{
				if (!_startService)
					break;
				if (!it->second->_ready)
					continue;
				if (it->second->_context)
				{
					THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
					libwebsocket_service(it->second->_context, 10);
				}
			}
	   }
	   Sleep(1);
	}
}

void CppWebsock::start()
{
	_startService = true;
	_serviceThread.create_thread(CppWebsock::_OnServiceLoop);
}

void CppWebsock::shutdown()
{
	_startService = false;
	_serviceThread.join_all();

	THREAD_LIB_LOCK_CRITICAL_SESSION(_mapLock)
	THREAD_LIB_LOCK_CRITICAL_SESSION(_addLock)
	THREAD_LIB_LOCK_CRITICAL_SESSION(_rmLock)
	while (_addToService.size() > 0)
	{
		ServiceInfo* info = _addToService.front();
		_addToService.pop_front();
		if (info) delete info;
	}
	while (_rmFromService.size() > 0)
	{
		ServiceInfo* info = _rmFromService.front();
		_rmFromService.pop_front();
		if (info) delete info;
	}
	for (std::map<int, CppWebsock*>::iterator it = _mapInstance.begin(); it != _mapInstance.end(); it++)
	{
		it->second->disconnect();
	}
	_mapInstance.clear();
}

void CppWebsock::disconnect()
{
	if (_context)
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_libwebLock)
		libwebsocket_context_destroy(_context);
	}
	_beClose = true;
	_curState = -1;
	_beWritable = false;
	_connected = false;
	_ready = false;
	_connectFailed = false;
	_context = NULL;	
}

int CppWebsock::read(unsigned char* buffer, int len, int timeout_ms)
{
	int n = 0;
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_readLock)

		int validDataSize = _recv.size();
		if (validDataSize >= len)
			n = len;
		else
			n = validDataSize;
		if (n > 0)
		{/*
			//============= TRACE =================//
			printf("buffer read[%d]: ", n);
			Byte* trace = _recv.begin();
			for (int i = 0; i < n; i++)
				printf("%c", *trace++);
			printf("\n");
			printf("remain:%d\n", validDataSize - n);
			//============= TRACE =================//*/			
			_recv.read(buffer, n);			
		}
	}
	return n;
}

int CppWebsock::write(unsigned char* buffer, int len, int timeout)
{	
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_sendLock)	
		_send.write((Byte*)buffer, len);		
	}
	return len;
}
