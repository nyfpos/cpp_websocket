#pragma once

#include "..\..\pch.h"

extern "C"
{
#include "packet/MQTTPacket.h"
#include "stdio.h"
}

#include "MQTTStream.h"

int MQTTSerialize_connectLengthEx(MQTTPacket_connectData* options);
int MQTTSerialize_subscribeLengthEx(int count, MQTTString topicFilters[]);
int MQTTSerialize_unsubscribeLengthEx(int count, MQTTString topicFilters[]);
int MQTTSerialize_publishLengthEx(int qos, MQTTString topicName, int payloadlen);
int MQTTSerialize_ack_ex(MQTTStream& stream, unsigned char packettype, unsigned char dup, unsigned short packetid);
int MQTTSerialize_pingreq_ex(MQTTStream& stream);
int MQTTSerialize_connect_ex(MQTTStream& stream, MQTTPacket_connectData* options);
int MQTTSerialize_subscribe_ex(MQTTStream& stream, unsigned char dup, unsigned short packetid, int count, MQTTString topicFilters[], int requestedQoSs[]);
int MQTTSerialize_unsubscribe_ex(MQTTStream& stream, unsigned char dup, unsigned short packetid, int count, MQTTString topicFilters[]);
int MQTTSerialize_publish_ex(MQTTStream& stream, unsigned char dup, int qos, unsigned char retained, unsigned short packetid, MQTTString topicName, unsigned char* payload, int payloadlen);
int MQTTSerialize_disconnect_ex(MQTTStream& stream);