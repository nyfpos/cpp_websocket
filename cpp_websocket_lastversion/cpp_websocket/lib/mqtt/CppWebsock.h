#pragma once

extern "C"
{
#include "lws_config.h"
#include "libwebsockets.h"
#include "util\getopt.h"
#include "util\gettimeofday.h"
}

#include <sys/types.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <map>
#include <list>
#include <vector>
#include "..\..\pch.h"
#include <thread>
#include <atomic>
#include <boost/thread/thread.hpp>
#include "util\Lock.h"

class WsStream
{
private:
	Byte* _begin;
	Byte* _current;
	int _size;

public:
	WsStream(const int payloadSize) :_begin(NULL), _current(NULL), _size(0)
	{
		init(payloadSize);
	}
	~WsStream()
	{
		if (_begin)
			delete[] _begin;
	}

	void init(const int payloadSize)
	{		
		const int SIZE = LWS_SEND_BUFFER_PRE_PADDING + payloadSize + LWS_SEND_BUFFER_POST_PADDING;
		_size = payloadSize;
		_begin = new Byte[SIZE];
		memset(_begin, 0, SIZE*sizeof(Byte));
		_current = begin();		
	}
	Byte* begin()
	{
		return _begin + LWS_SEND_BUFFER_PRE_PADDING;
	}
	Byte* end()
	{
		return _begin + LWS_SEND_BUFFER_PRE_PADDING + _size;
	}

	int size()
	{
		return _current - begin();
	}

	int capacity()
	{
		return _size;
	}

	void capacity(int len)
	{
		Byte* ed = end();
		Byte* st = begin();
		int remain = ed - _current;
		if (remain < len)
		{
			printf("[WsStream.write] buffer resize...\n");
			int validDataSize = _current - st;
			int append = (len - remain) * 2;
			int currentSize = ed - st;
			init(currentSize + append);
			Byte* newBegin = begin();
			memcpy_s(newBegin, validDataSize, st, validDataSize);
			delete[] st;
			_current = newBegin + validDataSize;
			printf("[WsStream.write] buffer resize done\n");
		}
	}

	void write(Byte* data, int len)
	{		
		capacity(len);
		memcpy_s(_current, len, data, len);
		_current += len;
	}

	int read(Byte* Dst, int n)
	{
		Byte* st = begin();
		Byte* ed = end();
		int validDataSize = _current - st;
		if (validDataSize > 0 &&
			validDataSize >= n)
		{
			int remain = validDataSize - n;
			memcpy_s(Dst, n, st, n);
			Byte* newBegin = st + n;
			for (int i = 0; i < remain; i++)
			{
				*(st + i) = *(newBegin + i);
			}
			_current = st + remain;
			memset(_current, 0, ed - _current);
			return n;
		}
		return 0;
	}
};

struct ServiceInfo;

class CppWebsock
{		
private:
	struct libwebsocket_context *_context;		
	bool _beClose;
	bool _beWritable;
	int _curState;	

	static swlib::CriticalSession _mapLock;
	static swlib::CriticalSession _libwebLock;
	swlib::CriticalSession _sendLock;
	swlib::CriticalSession _readLock;

	static std::map<int, CppWebsock*> _mapInstance;
	
	WsStream _send;
	WsStream _recv;

	std::atomic<bool> _connected;	
	std::atomic<bool> _ready;
	std::atomic<bool> _connectFailed;

	int _OnWsESTABLISHED(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);
	int _OnWsCLOSED(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);
	int _OnWsRECEIVE(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);
	int _OnWsWRITEABLE(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);	
	int _OnWsConnError(struct libwebsocket_context *context, struct libwebsocket *wsi, void *user, void *in, size_t len);	
	static void _OnServiceLoop();
	

	static swlib::CriticalSession _addLock;
	static swlib::CriticalSession _rmLock;
	static std::list<ServiceInfo*> _addToService;
	static std::list<ServiceInfo*> _rmFromService;
	static std::atomic<bool> _startService;
	static boost::thread_group _serviceThread;	

public:
	CppWebsock();
	~CppWebsock();
	bool isConnected() const { return _connected; }
	bool isConnectFailed() const { return _connectFailed; }
	int connect(const char *address, int port, int ssl_connection, const char *path, const char *host, const char *origin, int ietf_version_or_minus_on);
	int read(unsigned char* buffer, int len, int timeout_ms);	
	int write(unsigned char* buffer, int len, int timeout);
	void disconnect();


	static void start();
	static void shutdown();
	static CppWebsock* getInstance(struct libwebsocket *wsi);
	static int callback_lws(struct libwebsocket_context *context,
	                        struct libwebsocket *wsi,
	                        enum libwebsocket_callback_reasons reason,
		                    void *user, void *in, size_t len);	
};

struct ServiceInfo
{
	ServiceInfo(CppWebsock* ws_, int socket_) :ws(ws_), socket(socket_) {}
	CppWebsock* ws;
	int socket;
};

class Countdown
{
private:
	int gettimeofday(struct timeval * tp, struct timezone * tzp)
	{
		const unsigned __int64 epoch = ((unsigned __int64)116444736000000000ULL);
		FILETIME    file_time;
		SYSTEMTIME  system_time;
		ULARGE_INTEGER ularge;

		GetSystemTime(&system_time);
		SystemTimeToFileTime(&system_time, &file_time);
		ularge.LowPart = file_time.dwLowDateTime;
		ularge.HighPart = file_time.dwHighDateTime;

		tp->tv_sec = (long)((ularge.QuadPart - epoch) / 10000000L);
		tp->tv_usec = (long)(system_time.wMilliseconds * 1000);

		return 0;
	}

	void timersub(const struct timeval* tvp, const struct timeval* uvp, struct timeval* vvp)
	{
		vvp->tv_sec = tvp->tv_sec - uvp->tv_sec;
		vvp->tv_usec = tvp->tv_usec - uvp->tv_usec;
		if (vvp->tv_usec < 0)
		{
			--vvp->tv_sec;
			vvp->tv_usec += 1000000;
		}
	}

	void timeradd(const struct timeval* tvp, const struct timeval* uvp, struct timeval* vvp)
	{
		vvp->tv_sec = tvp->tv_sec + uvp->tv_sec;
		vvp->tv_usec = tvp->tv_usec + uvp->tv_usec;
		if (vvp->tv_usec >= 1000000)
		{
			++vvp->tv_sec;
			vvp->tv_usec -= 1000000;
		}
	}

public:
	Countdown()
	{

	}

	Countdown(int ms)
	{
		countdown_ms(ms);
	}


	bool expired()
	{
		struct timeval now, res;
		gettimeofday(&now, NULL);
		timersub(&end_time, &now, &res);
		//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
		//if (res.tv_sec > 0 || res.tv_usec > 0)
		//	printf("expired %d %d\n", res.tv_sec, res.tv_usec);
		return res.tv_sec < 0 || (res.tv_sec == 0 && res.tv_usec <= 0);
	}


	void countdown_ms(int ms)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		struct timeval interval = { ms / 1000, (ms % 1000) * 1000 };
		//printf("interval %d %d\n", interval.tv_sec, interval.tv_usec);
		timeradd(&now, &interval, &end_time);
	}


	void countdown(int seconds)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		struct timeval interval = { seconds, 0 };
		timeradd(&now, &interval, &end_time);
	}


	int left_ms()
	{
		struct timeval now, res;
		gettimeofday(&now, NULL);
		timersub(&end_time, &now, &res);
		//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
		return (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000;
	}

private:

	struct timeval end_time;
};

