var WebSocket = require("ws");
var ws = new WebSocket('ws://127.0.0.1:7681');//('wss://echo.websocket.org');
var fs = require("fs");

var test_json = [
fs.readFileSync("./test0.json"),
fs.readFileSync("./test1.json"),
fs.readFileSync("./test2.json")];

var id = Date.now().toString();

var j = 0;
var _send = function()
{
	try {		
		ws.send(test_json[j++]);
		j%=test_json.length;
	} catch(ex) {
		console.log(ex);
	}	
}

ws.on('open', function open() {

 // ws.send("hi");
  //process.exit(0);
 // console.log(ws)
	setInterval(_send, 3000);
  
});

ws.on('close', function close() {
  console.log('disconnected');
});

var i = 0;
ws.on('message', function(data) {
  // flags.binary will be set if a binary data is received.
  // flags.masked will be set if the data was masked.
  // console.log('received: ', data.toString());  
  fs.writeFileSync('tmp\\client['+id+"]"+(++i)+'.json', new Buffer(data));
});